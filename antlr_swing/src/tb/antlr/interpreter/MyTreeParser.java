package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	public LocalSymbols localSymbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected int divide(int number1, int number2) {
		if(number2 == 0) {
			throw new ArithmeticException("Divided by 0");
		}
		return number1 / number2;
	}
	
	protected int pow(int number, int power) {
		return (int) Math.pow(number, power);
	}
	
	protected int modulo(int number1, int number2) {
		return number1 % number2;
	}
}
