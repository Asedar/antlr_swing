tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = pow($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = modulo($e1.out, $e2.out);}
        | ^(PODST id=ID   e1=expr) {localSymbols.setSymbol($id.text, $e1.out);}
        | ^(PRINT e1=expr)         {drukuj($e1.out.toString());}
        | ^(VAR   id=ID)           {localSymbols.newSymbol($id.text);}
        | ID                       {$out = localSymbols.getSymbol($ID.text);}
        | LB                       {localSymbols.enterScope();}
        | RB                       {localSymbols.leaveScope();}
        | INT                      {$out = getInt($INT.text);}
        ;
